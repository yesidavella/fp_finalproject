#!/usr/bin/env stack

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}

module FetchData
    (
    getAuthors,
    getBooks
    ) where

import qualified Data.ByteString.Lazy.Char8 as S8
import qualified Data.ByteString.Lazy as LBS
import Data.Aeson
import Network.HTTP.Conduit
import qualified Control.Exception as E
import Control.Monad

import DataStructures

-- | gets the authors from the given url using http get request
getAuthors :: String -> IO (Either String Authors)
getAuthors url =  fmap eitherDecode $ ((simpleHttp $ url) `E.catch` statusExceptionHandler)

-- | gets the books from the given url using http get request
getBooks :: String -> IO (Either String Books)
getBooks url =  fmap eitherDecode $ ((simpleHttp $ url) `E.catch` statusExceptionHandler)

-- | Handles any exceptions that may rise during the http get request
statusExceptionHandler :: E.SomeException -> IO (LBS.ByteString)
statusExceptionHandler e = (putStrLn "Error while fetching data") >> (return S8.empty)