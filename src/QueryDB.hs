{-# LANGUAGE OverloadedStrings #-}

module QueryDB
    (
    createDBConn,
    initDBTables,
    insertAuthors,
    insertBooks,
    printScreenAuthors,
    printScreenBooks,
    queryBooks,
    queryAuthors,
    printAuthorByLastName,
    printBooksByRating,
    printBookWithAuthorsInfo,
    printBooksByAuthorLastName,
    deleteAuthorByLastName,
    deleteBookByISBN
    ) where

import GHC.Int
import Database.SQLite3
import Data.Text (pack, unpack)
import DataStructures
import InitInsertData

import System.Environment
import System.IO
import System.IO.Error

import Control.Exception
---------------------------------------Query Author table---------------------------------------
{-| It handles any possible sql error during Author deletion process -}
authorDeleteHandler :: SQLError -> IO ()
authorDeleteHandler e = putStrLn $ "Msg from dev team: Delete BOOKS regarding this Author first and then try again. Process CANCELLED. Reason: " ++ show(e)


{-| It deletes the Author by exactly matching last name -}
deleteAuthorByLastName :: Database -> String -> IO ()
deleteAuthorByLastName dbHandler lastName = do
  row <- execPrint dbHandler (pack $ "delete from AUTHOR where last_name='"++ lastName ++"'") `catch` authorDeleteHandler
  print $ "Delete Author process finished."


{-| It queries and then print on screen all existing Authors by Last Name -}
printAuthorByLastName :: Database -> String -> IO ()
printAuthorByLastName dbHandler lastName = do
  authorsArray <- queryAuthorsWithSQLSentence dbHandler $ "SELECT * FROM AUTHOR WHERE last_name LIKE '%" ++ lastName ++ "%'"
  mapM_ print authorsArray


{-| It queries DB Author's table who matches with the sql sentence -}
queryAuthorsWithSQLSentence :: Database -> String -> IO [Author]
queryAuthorsWithSQLSentence dbHandler sqlSentece= do
  stmt <- prepare dbHandler (pack sqlSentece)
  authorsArray <- iterateOverResultSet stmt []
  return authorsArray


{-| It queries and then print on screen all existing Authors -}
printScreenAuthors :: Database -> IO ()
printScreenAuthors dbHandler = do
  authorsArray <- queryAuthors dbHandler
  mapM_ print authorsArray


{-| It queries all DB Authors -}
queryAuthors :: Database -> IO [Author]
queryAuthors dbHandler = do
  stmt <- prepare dbHandler (pack "SELECT * FROM AUTHOR WHERE 1=1")
  authorsArray <- iterateOverResultSet stmt []
  return authorsArray


{-| It checks whether the resulset has more db data -}
isSQLValid (SQLNull) = False
isSQLValid _ = True


{-| It Iterates over all the Author's table -}
iterateOverResultSet :: Statement -> [Author] ->IO [Author]
iterateOverResultSet stmt authorArray = do
  record <- step stmt
  testValue <- column stmt 0

  if (isSQLValid testValue) then do
    code <- columnText stmt 0
    firstName <- columnText stmt 1
    lastName <- columnText stmt 2
    numOfBooks <- columnInt64 stmt 3
    gender <- columnText stmt 4
    dob <- columnText stmt 5
    let newAuthor = Author (unpack code) (unpack firstName) (unpack lastName) (numOfBooks::Int64) (unpack gender) (unpack dob)
    iterateOverResultSet stmt (newAuthor:authorArray)
  else
    return authorArray

----------------------------------------Query Book table-----------------------------------------

{-| It queries and then print on screen all existing Books by the requested Rating -}
printBooksByRating :: Database -> String -> IO ()
printBooksByRating dbHandler bookRating = do
  authorsArray <- queryBooksWithSQLSentence dbHandler $ "SELECT * FROM BOOK WHERE rating = "++ bookRating ++" ORDER BY title desc;"
  mapM_ print authorsArray

{-| It queries DB Book's table which matches with the sql sentence -}
queryBooksWithSQLSentence :: Database -> String -> IO [Book]
queryBooksWithSQLSentence dbHandler sqlSentence = do
  stmt <- prepare dbHandler (pack sqlSentence)
  bookArray <- iterateOverBookResultSet stmt []
  return bookArray


{-| It queries and then print on screen all existing Books -}
printScreenBooks :: Database -> IO ()
printScreenBooks dbHandler = do
  bookArray <- queryBooks dbHandler
  mapM_ print bookArray


{-| It queries all DB Books -}
queryBooks :: Database -> IO [Book]
queryBooks dbHandler = do
  stmt <- prepare dbHandler (pack "SELECT * FROM BOOK WHERE 1=1")
  bookArray <- iterateOverBookResultSet stmt []
  return bookArray


{-| It Iterates over all the Book's table -}
iterateOverBookResultSet :: Statement -> [Book] ->IO [Book]
iterateOverBookResultSet stmt bookArray = do
  record <- step stmt
  testValue <- column stmt 0

  if (isSQLValid testValue) then do
    id <- columnInt64 stmt 0
    isbn <- columnText stmt 1
    title <- columnText stmt 2
    author_code <- columnText stmt 3
    publication_date <- columnText stmt 4
    language <- columnText stmt 5
    rating <- columnInt64 stmt 6
    let newBook = Book (id :: Int64) (unpack isbn) (unpack title) (unpack author_code) (unpack publication_date) (unpack language) (rating :: Int64)
    iterateOverBookResultSet stmt (newBook:bookArray)
  else
    return bookArray


{-| It handles any possible sql error during book deletion process -}
bookDeleteHandler :: SQLError -> IO ()
bookDeleteHandler e = putStrLn $ "Msg from dev team: Delete Process CANCELLED. Reason:" ++ show(e)


{-| It deletes the Book by exactly matching ISBN code -}
deleteBookByISBN :: Database -> String -> IO ()
deleteBookByISBN dbHandler isbn = do
  row <- execPrint dbHandler (pack $ "delete from BOOK where isbn='"++ isbn ++"'") `catch` bookDeleteHandler
  print $ "Delete Book process finished."

----------------------------------------Join query on Book and Author's tables-----------------------------------------
{-| It prints and queries all the db books by author's last name prompted from console by user -}
printBooksByAuthorLastName :: Database -> String -> IO ()
printBooksByAuthorLastName dbHandler authorLastName = do
  authorsArray <- queryBooksWithAuthorInfoSQLSentence dbHandler $ "SELECT B.*, A.last_name, A.first_name FROM BOOK B INNER JOIN AUTHOR A ON (B.author_code=A.code) WHERE A.last_name LIKE '%"++ authorLastName ++"%' ORDER BY A.last_name DESC, B.title desc;"
  mapM_ print authorsArray

{-| It prints and queries all the db books info with last and first author's name -}
printBookWithAuthorsInfo :: Database -> IO ()
printBookWithAuthorsInfo dbHandler = do
  authorsArray <- queryBooksWithAuthorInfoSQLSentence dbHandler $ "SELECT B.*, A.last_name, A.first_name FROM BOOK B LEFT JOIN AUTHOR A ON (B.author_code=A.code) ORDER BY B.title desc;"
  mapM_ print authorsArray

{-| It queries all the db books info with last and first author's name -}
queryBooksWithAuthorInfoSQLSentence :: Database -> String -> IO [JoinedBookAuthor]
queryBooksWithAuthorInfoSQLSentence dbHandler sqlSentence = do
  stmt <- prepare dbHandler (pack sqlSentence)
  bookArray <- iterateOverBookAuthorInfoResultSet stmt []
  return bookArray

{-| It Iterates over all the Book's and Author's tables -}
iterateOverBookAuthorInfoResultSet :: Statement -> [JoinedBookAuthor] ->IO [JoinedBookAuthor]
iterateOverBookAuthorInfoResultSet stmt bookAuthorArray = do
  record <- step stmt
  testValue <- column stmt 0

  if (isSQLValid testValue) then do
    id <- columnInt64 stmt 0
    isbn <- columnText stmt 1
    title <- columnText stmt 2
    author_code <- columnText stmt 3
    publication_date <- columnText stmt 4
    language <- columnText stmt 5
    rating <- columnInt64 stmt 6
    authorLastName <- columnText stmt 7
    authorFirstName <- columnText stmt 8

    let newBook = Book (id :: Int64) (unpack isbn) (unpack title) (unpack author_code) (unpack publication_date) (unpack language) (rating :: Int64)
    let newBookAuthorJoinedInfo = JoinedBookAuthor newBook (unpack authorLastName) (unpack authorFirstName)
    iterateOverBookAuthorInfoResultSet stmt (newBookAuthorJoinedInfo:bookAuthorArray)
  else
    return bookAuthorArray