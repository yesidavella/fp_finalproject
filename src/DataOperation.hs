module DataOperation (calculateAvgRatingPerAuthor) where


import Data.Map.Strict as M
import DataStructures
import GHC.Int
import Control.Applicative
import Database.SQLite3
import QueryDB
import Data.List
import Data.Ord
import Control.Monad (forM_)

-- | Prints multiple element lists
printTuples ::[(String, String, String, Double)] -> IO ()
printTuples xs = forM_ xs (putStrLn . formatOne)

-- | Takes multiple elements from a list element and converts them into a String
formatOne :: (String, String, String, Double) -> String
formatOne (c,f,l,avg) = c ++ " , " ++ f ++ " " ++ l ++ " , Avg Rating: " ++ show avg

-- | Returns a Map from a list of books with (k,v) = (author_code, 0)
booksMap :: [Book] -> Map String Int64
booksMap books = M.fromList $ Prelude.map (\c -> (author_code c, 0)) books

-- | calculates the Average book rating for each author and prints it by descending order
calculateAvgRatingPerAuthor :: Database -> IO ()
calculateAvgRatingPerAuthor dbHandler = do
        books <- queryBooks dbHandler
        authors <- queryAuthors dbHandler
        let books' = booksMap books
        let countMap = iterateThroughBooks books books'
        let countList = M.toList countMap
        let sumRatingMap = iterateThroughBooks2 books books'
        let sumRatingList = M.toList sumRatingMap
        let avgList = calculateAvg authors countMap sumRatingMap
        let sortedList = sortBy (flip . comparing $ extractAvg) avgList
        printTuples sortedList
        return ()

-- | calculates the final average book rating for each author code
calculateAvg :: [Author] -> Map String Int64 -> Map String Int64 -> [(String, String, String, Double)]
calculateAvg [] countMap ratingMap = []
calculateAvg (x:xs) countMap ratingMap = (code x, first_name x, last_name x, fromIntegral((findWithDefault 0 (code x) ratingMap)) / fromIntegral((findWithDefault 1 (code x) countMap))):(calculateAvg xs countMap ratingMap)

-- | Iterates through the books list and creates a map with (k,v) = (author_code, sumOfRatings) where sumOfRatings the summation of all the book ratings for each author
iterateThroughBooks2 :: [Book] -> Map String Int64 -> Map String Int64
iterateThroughBooks2 [] map = map
iterateThroughBooks2 (x:xs) map = iterateThroughBooks2 xs (updateMapPF (author_code x) (rating x) $ map)

-- | Iterates through the books list and creates a map with (k,v) = (author_code, count) where count the number of book for each author code
iterateThroughBooks :: [Book] -> Map String Int64 -> Map String Int64
iterateThroughBooks [] map = map
iterateThroughBooks (x:xs) map = iterateThroughBooks xs (updateMapPF (author_code x) 1 $ map)

-- | Updates the map using the + operation
updateMapPF ::  String -> Int64 -> Map String Int64 -> Map String Int64
updateMapPF = M.insertWith (+)

-- | extracts the average from the multiple element list
extractAvg :: (String,String,String,Double) -> Double
extractAvg (_,_,_,d) = d