#!/usr/bin/env stack

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}

module ParseData
    (
    fetchData
    ) where

import qualified Data.ByteString.Lazy.Char8 as S8
import qualified Data.ByteString.Lazy as LBS
import Data.Aeson
import GHC.Generics
import Network.HTTP.Conduit
import Data.Text (Text)
import qualified Control.Exception as E
import System.Exit (exitSuccess)
import Control.Monad

import DataStructures
import InitInsertData
import Database.SQLite3
import DumpDB
import FetchData

-- | The fetchData function is used is used to call the functions that retrieve the data, parse them into objects and put them in the database
fetchData :: Database -> IO ()
fetchData dbHandler = do
        response <- getAuthors "https://my.api.mockaroo.com/authors.json?key=eb935d60"
        if (not (isResponseOK(response))) then putStrLn(show (response)) else prepareAuthors response dbHandler

        response <- getBooks "https://my.api.mockaroo.com/books.json?key=eb935d60"
        if (not (isResponseOK(response))) then putStrLn(show (response)) else prepareBooks response dbHandler
        print "Good news!!! Download process done successfully!!! Check Database!!!"

-- | Checks if Either response is left or right
isResponseOK :: Either a b -> Bool
isResponseOK (Left a) = False
isResponseOK (Right b) = True

-- | Prepares the Authors data structures and calls the function that will insert the data into the DB
prepareAuthors :: (Either a Authors) -> Database -> IO ()
prepareAuthors response dbHandler = do
                   let response' = eliminateEither(response)
                   let authorslist = eliminateAuthors(response')
                   insertAuthors dbHandler authorslist

-- | Prepares the Books data structures and calls the function that will insert the data into the DB
prepareBooks :: (Either a Books) -> Database -> IO ()
prepareBooks response dbHandler = do
                   let response' = eliminateEither(response)
                   let booklist = eliminateBooks(response')
                   insertBooks dbHandler booklist

-- | Eliminates the Right and returns just the value
eliminateEither :: Either a b -> b
eliminateEither (Right b) = b

-- | Eliminates the Authors from the Authors [author] and returns just an array of authors: [author]
eliminateAuthors :: Authors -> [Author]
eliminateAuthors (Authors a) = a

-- | Eliminates the Books from the Books [book] and returns just an array of books: [book]
eliminateBooks :: Books -> [Book]
eliminateBooks (Books a) = a