{-# LANGUAGE OverloadedStrings #-}

module InitInsertData
    (
    createDBConn,
    initDBTables,
    insertAuthors,
    insertBooks
    ) where

import GHC.Int
import Database.SQLite3
import Data.Text (pack)
import DataStructures

{-| It create only the DB connection, returns a DB handler -}
createDBConn :: IO Database
createDBConn = open $ pack "bookstore.db"

{-| It creates 2 tables, AUTHOR (one) AND BOOK(to many) -}
initDBTables :: Database -> IO ()
initDBTables db = do
   exec db $ pack "CREATE TABLE IF NOT EXISTS AUTHOR (\
            \code VARCHAR(40) PRIMARY KEY, \
            \first_name VARCHAR(40) NOT NULL, \
            \last_name VARCHAR(40) NOT NULL, \
            \num_of_books SMALLINT DEFAULT NULL, \
            \gender VARCHAR(40) DEFAULT NULL, \
            \dob DATE DEFAULT NULL)"

   exec db $ pack "CREATE TABLE IF NOT EXISTS BOOK (\
            \book_id INTEGER PRIMARY KEY AUTOINCREMENT, \
            \isbn VARCHAR(17) NOT NULL, \
            \title VARCHAR(40) NOT NULL, \
            \author_code VARCHAR(40) NOT NULL, \
            \publication_date DATE NOT NULL, \
            \language VARCHAR(40) DEFAULT NULL, \
            \rating SMALLINT DEFAULT NULL, \
            \FOREIGN KEY (author_code) REFERENCES AUTHOR(code) ON DELETE RESTRICT)"

   exec db $ pack "PRAGMA foreign_keys = ON"

{-| It inserts into the db all the authors stored in the array, based on single inserts -}
insertAuthors :: Database -> [Author] -> IO ()
insertAuthors db authorArray = do
  mapM_ (insertOneAuthor db) authorArray

{-| It inserts only one author into the DB -}
insertOneAuthor :: Database -> Author -> IO ()
insertOneAuthor db oneAuthor = do
  let f (Author code first_name last_name num_of_books gender dob) = [
                                     (pack ":code", SQLText (pack code))
                                   , (pack ":first_name", SQLText (pack first_name))
                                   , (pack ":last_name", SQLText (pack last_name))
                                   , (pack ":num_of_books", SQLInteger num_of_books)
                                   , (pack ":gender", SQLText (pack gender))
                                   , (pack ":dob", SQLText (pack dob))]
  let args = f oneAuthor
  stmt <- prepare db (pack "INSERT INTO AUTHOR VALUES (:code,:first_name,:last_name,:num_of_books,:gender,:dob)")
  bindNamed stmt (f oneAuthor)
  result <- step stmt
  return ()

{-| It inserts into the db all the books stored in the array, based on single inserts -}
insertBooks :: Database -> [Book] -> IO ()
insertBooks db bookArray = do
  mapM_ (insertOneBook db) bookArray


{-| It inserts only one book into the DB -}
insertOneBook :: Database -> Book -> IO ()
insertOneBook db oneBook = do
  let f (Book book_id isbn title author_code publication_date language rating) = [
                                       (pack ":isbn", SQLText (pack isbn))
                                     , (pack ":title", SQLText (pack title))
                                     , (pack ":author_code", SQLText (pack author_code))
                                     , (pack ":publication_date", SQLText (pack publication_date))
                                     , (pack ":language", SQLText (pack language))
                                     , (pack ":rating", SQLInteger rating)]
  let args = f oneBook
  stmt <- prepare db (pack "INSERT INTO BOOK (isbn,title,author_code,publication_date,language,rating) VALUES (:isbn,:title,:author_code,:publication_date,:language,:rating)")
  bindNamed stmt (f oneBook)
  result <- step stmt
  return ()