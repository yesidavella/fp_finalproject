{-# LANGUAGE OverloadedStrings #-}

module DumpDB
    (
    dumpDBToJson
    ) where

import GHC.Int
import Database.SQLite3
import Data.Text as Text1 (pack, unpack)

import DataStructures
import Data.ByteString.Lazy.Char8 as Char8
import Data.Aeson
import GHC.Generics

import QueryDB
import InitInsertData

--------------------------------------Dumping Author and Book table into json files-------------------------------------

-- | Retrieves all the author and book data and writes them to json files with the names authors.json and books.json -
dumpDBToJson :: Database -> IO ()
dumpDBToJson dbHandler = do
  authorsArray <- queryAuthors dbHandler
  let authorsJSON = Authors authorsArray
  Char8.writeFile "authors.json" (encode authorsJSON)
  print "Good news!!! authors.json file created in root project folder successfully!!!"
  booksArray <- queryBooks dbHandler
  let booksJSON = Books booksArray
  Char8.writeFile "books.json" (encode booksJSON)
  print "Good news!!! book.json file created in root project folder successfully!!!"