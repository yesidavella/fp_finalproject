{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}

module DataStructures (Author(..)
                           ,Book(..)
                           ,Books(..)
                           ,Authors(..),
                           JoinedBookAuthor(..)) where

import Data.Aeson
import GHC.Generics
import GHC.Int

-- | represents the joined books and authors
data JoinedBookAuthor = JoinedBookAuthor {book :: Book,
                                          author_last_name :: String,
                                          author_first_name :: String} deriving (Show)
-- | represents a single book
data Book = Book {book_id :: Int64,
                  isbn :: String,
                  title :: String,
                  author_code :: String,
                  publication_date :: String,
                  language :: String,
                  rating :: Int64} deriving (Show, Generic, Ord)

instance FromJSON Book
instance ToJSON Book

instance Eq Book where
  book1 == book2 = (book_id book1) == (book_id book2)

-- | Represents the books that we parse from the api
data Books = Books [Book] deriving (Show, Generic)


instance FromJSON Books
instance ToJSON Books

-- | Represents a single author
data Author = Author {code :: String,
                       first_name :: String,
                       last_name :: String,
                       num_of_books :: Int64,
                       gender :: String,
                       dob :: String
                      } deriving (Show, Generic, Ord)

instance FromJSON Author
instance ToJSON Author

instance Eq Author where
  author1 == author2 = (code author1) == (code author2)

-- | Represents all the authors that we parse from the api
data Authors = Authors [Author] deriving (Show, Generic)

instance FromJSON Authors
instance ToJSON Authors