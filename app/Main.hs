{-# LANGUAGE OverloadedStrings #-}
module Main where

import System.Exit (exitSuccess)
import Database.SQLite3
import DumpDB
import ParseData
import InitInsertData
import QueryDB
import DataOperation


import System.Environment
import System.IO
import System.IO.Error
import Control.Exception.Base

{-| It is the main method to execute the Haskell app -}
main :: IO ()
main = do

  dbHandler <- createDBConn
  initDBTables dbHandler
  showMainUserMenu dbHandler


{-| It shows the Main Menu to the user -}
showMainUserMenu dbHandler = do
  putStrLn $ "**********MAIN USER OPTION MENU**********"
  putStrLn $ "Type (1) to download and insert DB data"
  putStrLn $ "Type (2) to show Author menu table"
  putStrLn $ "Type (3) to show Book menu table"
  putStrLn $ "Type (4) to export DB to JSON"
  putStrLn $ "Type (0) exit program"

  typedOption <- getLine
  executeMainMenuOption dbHandler typedOption
  showMainUserMenu dbHandler

{-| It shows the Book Menu to the user -}
showBookMenu dbHandler = do
  putStrLn $ "**********BOOK OPTION MENU**********"
  putStrLn $ "Type (1) to query all Book table"
  putStrLn $ "Type (2) to query books with author's information"
  putStrLn $ "Type (3) to query books for author's last name"
  putStrLn $ "Type (4) to query books by rating."
  putStrLn $ "Type (5) to DELETE Book for ISBN from DB."
  putStrLn $ "Type (0) to go back to Main Manu."

  typedOption <- getLine
  executeBookOption dbHandler typedOption
  showBookMenu dbHandler

{-| It shows the Author Menu to the user -}
showAuthorMenu dbHandler = do
  putStrLn $ "**********AUTHOR OPTION MENU**********"
  putStrLn $ "Type (1) to query all Author table"
  putStrLn $ "Type (2) to view Average book rating for every author in descending order"
  putStrLn $ "Type (3) to query Author for Last Name."
  putStrLn $ "Type (4) to DELETE Author by Last Name from DB."
  putStrLn $ "Type (0) to go back to Main Manu"

  typedOption <- getLine
  executeAuthorOption dbHandler typedOption
  showAuthorMenu dbHandler


{-| It redirects the Main Menu typed user option to the concrete action -}
executeMainMenuOption :: Database -> String -> IO ()
executeMainMenuOption dbHandler userOption
   | userOption == "0"   = exitSuccess
   | userOption == "1"   = fetchData dbHandler
   | userOption == "2"   = showAuthorMenu dbHandler
   | userOption == "3"   = showBookMenu dbHandler
   | userOption == "4"   = dumpDBToJson dbHandler
   | otherwise  = putStrLn "Read carefully dude!!! Press the correct option!!!"


{-| It redirects to Book option typed by user to a concrete action -}
executeBookOption :: Database -> String -> IO ()
executeBookOption dbHandler userOption
   | userOption == "1" = printScreenBooks dbHandler
   | userOption == "2" = printBookWithAuthorsInfo dbHandler
   | userOption == "3" = showPromptToFindBooksByAuthorLastName dbHandler
   | userOption == "4" = showPromptToFindBooksByRating dbHandler
   | userOption == "5" = showPromptToDeleteBookByISBN dbHandler
   | userOption == "0" = showMainUserMenu dbHandler
   | otherwise  = putStrLn "Read carefully dude!!! Press the correct option!!!"


{-| It redirects to Author option typed by user to a concrete action -}
executeAuthorOption :: Database -> String -> IO ()
executeAuthorOption dbHandler userOption
   | userOption == "1" = printScreenAuthors dbHandler
   | userOption == "2" = calculateAvgRatingPerAuthor dbHandler
   | userOption == "3" = showPromptToFindAuthorByLastName dbHandler
   | userOption == "4" = showPromptToDeleteAuthorByLastName dbHandler
   | userOption == "0" = showMainUserMenu dbHandler
   | otherwise  = putStrLn "Read carefully dude!!! Press the correct option!!!"

{-| It collects the Author's last name as input and shows the results on screen-}
showPromptToFindAuthorByLastName :: Database -> IO ()
showPromptToFindAuthorByLastName dbHandler = do
  putStrLn $ "Type the Author's last name you want to find:"
  typedOption <- getLine
  printAuthorByLastName dbHandler typedOption
  showAuthorMenu dbHandler

{-| It collects the Book's rating as input and show the results on screen-}
showPromptToFindBooksByRating :: Database -> IO ()
showPromptToFindBooksByRating dbHandler = do
  putStrLn $ "Type the Book's rating you want to find:"
  typedOption <- getLine
  printBooksByRating dbHandler typedOption
  showBookMenu dbHandler

{-| It collects the Book's Author's last name as input and show the results on screen-}
showPromptToFindBooksByAuthorLastName :: Database -> IO ()
showPromptToFindBooksByAuthorLastName dbHandler = do
  putStrLn $ "Type the Book's Author's last name you want to find:"
  typedOption <- getLine
  printBooksByAuthorLastName dbHandler typedOption
  showBookMenu dbHandler

{-| It collects the Book's Author's last name as input and show the results on screen-}
showPromptToDeleteAuthorByLastName :: Database -> IO ()
showPromptToDeleteAuthorByLastName dbHandler = do
  putStrLn $ "Type the Author's last name you want to DELETE:"
  authorLastName <- getLine
  (deleteAuthorByLastName dbHandler authorLastName)
  showAuthorMenu dbHandler


{-| It collects the Book's ISBN code as input to delete it -}
showPromptToDeleteBookByISBN :: Database -> IO ()
showPromptToDeleteBookByISBN dbHandler = do
  putStrLn $ "Type the Book's ISBN you want to DELETE:"
  booksIsbnCode <- getLine
  deleteBookByISBN dbHandler booksIsbnCode
  showBookMenu dbHandler